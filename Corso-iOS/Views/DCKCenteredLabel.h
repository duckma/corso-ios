//
//  DCKCenteredLabel.h
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCKCenteredLabel : UILabel

-(void)animaAbbassandoTrasparenza;
-(void)animaAlzandoTrasparenza;
-(void)descrizione;

@end
