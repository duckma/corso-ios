//
//  DCKCenteredLabel.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "DCKCenteredLabel.h"
#define DURATA_ANIMAZIONE 0.5f

@implementation DCKCenteredLabel

-(void)animaAbbassandoTrasparenza {
	
	[UIView animateWithDuration:DURATA_ANIMAZIONE animations:^{
	
		self.alpha = 0.5;
		
	}];
	
}

-(void)animaAlzandoTrasparenza {
	
	[UIView animateWithDuration:DURATA_ANIMAZIONE animations:^{
		
		self.alpha = 1.;
		
	}];
	
}

-(void)descrizione {
	
	NSLog(@"sono un label centrato");
	
}

@end
