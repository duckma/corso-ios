//
//  DCKInserimentoViewController.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "DCKInserimentoViewController.h"
#import "Citazione.h"

@interface DCKInserimentoViewController ()

@end

@implementation DCKInserimentoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
-(IBAction)aggiungiCitazione:(id)sender {
	
	// Check se sono stati riempiti i campi
	
	NSString *testoCitazione = [_testoTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSString *autoreCitazione = [_autoreTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	
	if ( [testoCitazione isEqualToString:@""] || [autoreCitazione isEqualToString:@""] ) {
		
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Testo o autore vuoti"
															message:@"Inserisci autore e citazione"
														   delegate:self
												  cancelButtonTitle:nil
												  otherButtonTitles:@"Ok", nil];
		[alertView show];
		
	} else {
		
		[self.delegate aggiungiCitazione:_testoTextView.text conAutore:_autoreTextField.text];
		[[self navigationController] popViewControllerAnimated:YES];
		
	}
	
}

@end
