//
//  DCKMenuTableViewController.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "DCKMenuTableViewController.h"
#import "DCKInserimentoViewController.h"
#import "DCKCitazioneViewController.h"
#import "DCKAppDelegate.h"
#import "Citazione.h"

@interface DCKMenuTableViewController ()

@end

@implementation DCKMenuTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Citazione"];
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dataInserimento" ascending:YES];
	fetchRequest.sortDescriptors = @[sortDescriptor];
	
	DCKAppDelegate *appDelegate = (DCKAppDelegate*)[[UIApplication sharedApplication] delegate];
	
	_objectContext = appDelegate.managedObjectContext;
	_citazioniResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
																	 managedObjectContext:_objectContext
																	   sectionNameKeyPath:nil
																				cacheName:nil];
	_citazioniResultController.delegate = self;
	[_citazioniResultController performFetch:nil];
	
}

- (void)viewWillAppear:(BOOL)animated {
	
	[self.tableView reloadData];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
	if (_citazioniResultController.fetchedObjects.count == 0) return 1;
    return 2;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
	// Return the number of rows in the section.
	if (section == 0) return 1;
    
	return _citazioniResultController.fetchedObjects.count;
	
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rigaDequeue" forIndexPath:indexPath];
    
    if (indexPath.row == 0 && indexPath.section == 0) {
		cell.textLabel.text = @"Inserisci nuova citazione";
		return cell;
	}
	
	// indexPath.row != 0 => indexPath.section == 1
	
	cell.textLabel.text = [NSString stringWithFormat:@"Citazione %d",indexPath.row + 1];
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	if (section == 0) return @"Inserisci";
	else return @"Leggi";
	
}

-(BOOL)aggiungiCitazione:(NSString*)citazione conAutore:(NSString*)autore {
	
	NSEntityDescription *citazioneEntity = [NSEntityDescription entityForName:@"Citazione" inManagedObjectContext:_objectContext];
	Citazione *cit = [[Citazione alloc] initWithEntity:citazioneEntity insertIntoManagedObjectContext:_objectContext];
	
	cit.testo = citazione;
	cit.autore = autore;
	cit.dataInserimento = [NSDate date];
	
	DCKAppDelegate *appDelegate = (DCKAppDelegate*)[[UIApplication sharedApplication] delegate];
	[appDelegate saveContext];
	
	return YES;
	
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.section == 0 && indexPath.row == 0) {
		
		if ([self shouldPerformSegueWithIdentifier:@"segueInserimento" sender:self]) {
			[self performSegueWithIdentifier:@"segueInserimento" sender:nil];
		}
		
	} else {
		
		if ([self shouldPerformSegueWithIdentifier:@"segueCitazione" sender:self]) {
			[self performSegueWithIdentifier:@"segueCitazione" sender:@{@"cit":[_citazioniResultController.fetchedObjects objectAtIndex:indexPath.row]}];
		}
		
	}
	
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
	if (indexPath.section == 1) return YES;
	return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	return UITableViewCellEditingStyleDelete;
	
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
		Citazione *citazioneDaRimuovere = [_citazioniResultController.fetchedObjects objectAtIndex:indexPath.row];
		[_objectContext deleteObject:citazioneDaRimuovere];
		DCKAppDelegate *appDelegate = (DCKAppDelegate*)[[UIApplication sharedApplication] delegate];
		[appDelegate saveContext];
		
		[tableView beginUpdates];
		[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
		if (_objectContext.registeredObjects.count == 0) [tableView deleteSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
		[tableView endUpdates];
    }
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

	if ([segue.identifier isEqualToString:@"segueInserimento"]) {
		DCKInserimentoViewController *destinazione = (DCKInserimentoViewController*)segue.destinationViewController;
		destinazione.delegate = self;
	}
	else if ([segue.identifier isEqualToString:@"segueCitazione"]) {
		
		DCKCitazioneViewController *visualizzazione = (DCKCitazioneViewController*)segue.destinationViewController;
		Citazione *citazione = [sender objectForKey:@"cit"];
				
		[visualizzazione impostaTesto:citazione.testo autore:citazione.autore];
		
	}
	
}


#pragma mark - Fetched results controller
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
	
    UITableView *tableView = self.tableView;
	
    switch(type) {
			
        case NSFetchedResultsChangeInsert: {
			NSIndexPath *indexPathInsert = [NSIndexPath indexPathForRow:indexPath.row inSection:1];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPathInsert] withRowAnimation:UITableViewRowAnimationFade];
		
            break;
		}
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeUpdate:
			[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
			
        case NSFetchedResultsChangeMove:
			[tableView deleteRowsAtIndexPaths:[NSArray
											   arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
			[tableView insertRowsAtIndexPaths:[NSArray
											   arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
			break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
	
    switch(type) {
			
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
			
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

@end
