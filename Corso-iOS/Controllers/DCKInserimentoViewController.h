//
//  DCKInserimentoViewController.h
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DCKMenuTableViewController.h"

@interface DCKInserimentoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *autoreTextField;
@property (strong, nonatomic) IBOutlet UITextView *testoTextView;
@property (weak, nonatomic) id <InserimentoCitazioniDelegate> delegate;

-(IBAction)aggiungiCitazione:(id)sender;

@end
