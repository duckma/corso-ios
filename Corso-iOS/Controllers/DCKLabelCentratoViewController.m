//
//  DCKLabelCentratoViewController.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "DCKLabelCentratoViewController.h"
#import "DCKCenteredLabel.h"

@interface DCKLabelCentratoViewController ()

@end

@implementation DCKLabelCentratoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_alzaAnimazione = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animaAlpha)];
	[tap setNumberOfTapsRequired:1];
	[self.view addGestureRecognizer:tap];
	
	_textViewCitazione.text = _citazione;
	_labelCentrato.text = _autore;
	
}

-(void)impostaTesto:(NSString*)testo autore:(NSString*)autore {
	
	_citazione = testo;
	_autore = autore;
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Animazioni
-(void)animaAlpha {
		
	if (_alzaAnimazione) [_labelCentrato animaAbbassandoTrasparenza];
	else [_labelCentrato animaAlzandoTrasparenza];
	
	_alzaAnimazione = !_alzaAnimazione;
	
}

@end
