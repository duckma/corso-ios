//
//  DCKLabelCentratoViewController.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "DCKCitazioneViewController.h"
#import "DCKCenteredLabel.h"

@interface DCKCitazioneViewController ()

@end

@implementation DCKCitazioneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_alzaAnimazione = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Do any additional setup after loading the view.
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(animaAlpha)];
	[tap setNumberOfTapsRequired:1];
	[self.view addGestureRecognizer:tap];
	
	_textViewCitazione.text = _citazione;
	_labelCentrato.text = _autore;
	
}

-(void)impostaTesto:(NSString*)testo autore:(NSString*)autore {
	
	_citazione = testo;
	_autore = autore;
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Animazioni
-(void)animaAlpha {
		
	if (_alzaAnimazione) [_labelCentrato animaAbbassandoTrasparenza];
	else [_labelCentrato animaAlzandoTrasparenza];
	
	_alzaAnimazione = !_alzaAnimazione;
	
}

#pragma mark - Cambia colore
-(IBAction)cambiaColore:(id)sender {
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@""
															 delegate:self
													cancelButtonTitle:@"Annulla"
											   destructiveButtonTitle:nil
													otherButtonTitles:@"Grigio/Azzurro", @"Arancio/Bianco", nil];
	[actionSheet showFromBarButtonItem:sender animated:YES];
	
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	
	if (buttonIndex == actionSheet.cancelButtonIndex) return;
	
	if (buttonIndex == 0) {
		// Grigio azzurro
		_labelCentrato.backgroundColor = [UIColor colorWithRed:0.180855F green:0.804180F blue:1.000000F alpha:1.0F];
		self.view.backgroundColor = [UIColor colorWithRed:0.591639F green:0.591639F blue:0.591639F alpha:1.0F];
	} else {
		// Arancio bianco
		_labelCentrato.backgroundColor = [UIColor colorWithRed:1.000000F green:0.263491F blue:0.230380F alpha:1.0F];
		self.view.backgroundColor = [UIColor whiteColor];
	}
	
}

@end
