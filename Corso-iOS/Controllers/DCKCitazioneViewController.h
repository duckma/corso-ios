//
//  DCKLabelCentratoViewController.h
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DCKCenteredLabel;

@interface DCKCitazioneViewController : UIViewController <UIActionSheetDelegate> {
	BOOL _alzaAnimazione;
}

@property (strong, nonatomic) IBOutlet DCKCenteredLabel *labelCentrato;
@property (strong, nonatomic) IBOutlet UITextView *textViewCitazione;
@property (strong, nonatomic) NSString *autore;
@property (strong, nonatomic) NSString *citazione;

-(void)impostaTesto:(NSString*)testo autore:(NSString*)autore;

@end
