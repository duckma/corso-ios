//
//  DCKMenuTableViewController.h
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InserimentoCitazioniDelegate <NSObject>
-(BOOL)aggiungiCitazione:(NSString*)citazione conAutore:(NSString*)autore;
@end

@interface DCKMenuTableViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate,InserimentoCitazioniDelegate,NSFetchedResultsControllerDelegate> {
	NSManagedObjectContext *_objectContext;
}

@property (strong, nonatomic) NSFetchedResultsController *citazioniResultController;

@end
