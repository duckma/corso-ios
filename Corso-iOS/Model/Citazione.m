//
//  Citazione.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import "Citazione.h"


@implementation Citazione

@dynamic testo;
@dynamic autore;
@dynamic tema;
@dynamic dataInserimento;

@end
