//
//  Citazione.h
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Citazione : NSManagedObject

@property (nonatomic, retain) NSString * testo;
@property (nonatomic, retain) NSString * autore;
@property (nonatomic, retain) NSDate *dataInserimento;
@property (nonatomic, retain) NSNumber * tema;

@end
