//
//  main.m
//  Corso-iOS
//
//  Created by Enrico Franzelli on 07/07/14.
//  Copyright (c) 2014 Duckma. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DCKAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([DCKAppDelegate class]));
	}
}
